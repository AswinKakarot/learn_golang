package main

import (
	"fmt"
	"os"
	"time"

	"github.com/inancgumus/screen"
	"github.com/mattn/go-runewidth"
	"golang.org/x/crypto/ssh/terminal"
)

const (
	cellBall  = '⚾'
	cellEmpty = ' '
	speed     = time.Second / 10
)

func printBoard(board [][]bool, buf []rune) []rune {
	// clear the screen once
	for i := range board {
		for j := range board[i] {
			var cell rune = cellEmpty
			if board[i][j] {
				cell = cellBall
			}
			buf = append(buf, cell, ' ')
		}
		buf = append(buf, '\n')
	}
	return buf
}

func getTermSize() (int, int) {
	w, h, err := terminal.GetSize(int(os.Stdout.Fd()))
	if err != nil {
		return 0, 0
	}
	return w, h
}
func main() {
	type position struct {
		x int
		y int
	}
	type velocity struct {
		x int
		y int
	}
	var (
		height int
		width  int
	)
	width, height = getTermSize()
	ballWidth := runewidth.RuneWidth(cellBall)
	width /= ballWidth
	height--
	curPos := position{0, 0}
	curVel := velocity{1, 1}
	board := make([][]bool, height)
	for i := range board {
		board[i] = make([]bool, width)
	}

	buf := make([]rune, 0, (width+1)*height)
	screen.Clear()
	for {
		buf = buf[:0]
		board[curPos.x][curPos.y] = false
		curPos.x += curVel.x
		curPos.y += curVel.y
		if curPos.x == height-1 || curPos.x == 0 {
			curVel.x *= -1
		}
		if curPos.y == width-1 || curPos.y == 0 {
			curVel.y *= -1
		}
		board[curPos.x][curPos.y] = true

		buf = printBoard(board, buf)
		screen.MoveTopLeft()
		fmt.Print(string(buf))
		// fmt.Printf("x: %d y: %d", curPos.x, curPos.y)
		// slow down the animation
		time.Sleep(speed)
	}
}
