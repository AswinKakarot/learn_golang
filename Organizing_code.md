# Naming Conventions
* package's name provides context for its contents.

# Choose a good import path
* Make your package go get-able
* If you don't use a hosted source repository, choose some unique prefix such as a domain, company, or project name.

# Minimize exported interface

Ref: https://blog.golang.org/organizing-go-code