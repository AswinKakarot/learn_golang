### Functions
* Modular programming methodology
* takes zero or more args
* can return zero or more values
* Can return named return values
  * return stamement in such functions is known as `naked` return
  * such functions should only be used in case of short functions since it will affect readability

Example:

:::code language="go" source="function_example/main.go":::