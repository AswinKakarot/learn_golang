package main

import (
	"fmt"
	"os"
	"strings"
)

const corpus = "" +
	"lazy cat jumps again and again and again"

func main() {
	query := os.Args[1:]
	for _, v := range query {
		fmt.Printf("%v occurs %d times\n", v, strings.Count(corpus, v))
	}
}
