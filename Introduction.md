# Introduction

Gopath
also known as workspace
get's automatically set by go

Directory Structure
src - source code
```bash
$GOPATH/src/repo-website/username-or-project/repo-name
```

DOB: `2009-11-10 23:00:00 UTC`// Helper file to hold the variables
## Documentation
```bash
go help command
```
List all env variable set// Helper file to hold the variables for go
```bash
go env
```
Package Docs can be found here https://golang.org/pkg/
# Running
```bash
go build
```
* Compile the code and produce the binary
```bash
go run
```
* Compile and run it directly
* Use -x opt to see debug logs like link logs

# Coding
## package clause
* Every go program is made up of packages
* A program start by running in `main` package
* first line of the program
* every go file can only belong to a single package
## func keyword
* function declaration
* main() function tells go where to start the execution

## fmt package
* short for formatting
* part of go standard library
* import package using import keyword

## Formatting code
```bash
go fmt

# syntax aware search and replace
# This makes large scale refactoring easier
go fmt -r 
```

## Concepts
### import
Import a package
### Exported Names
* A name is exported if it begins with a capital letter
* When importing a package you can only refer to Exported names

### Variables
* declared using the `var` stmt
* can be defined at package or func level
* can contain initializers one per var
* There is also a short variable declaration syntax using `:=`
* Variable declared without an explicit initial value are given their `zero value`
  * Zero Values are:
    * 0 for numerics
    * false for bool
    * "" string for strings

Example:
:::code language="go" source="var_example/main.go":::


#### Declaration Syntax
```go
x int
p *int
// Arrays
arr [3]int
// Functions
func function(argc int, argv []string) int{
}
// Closures
sum := func(a, b int) int { return a+b } (3, 4)
// Pointers
var p *int
x = *p
```

### Data types
#### Basic datatypes
* bool
* string
* int, int8, int16, int32, int64
* uint, uint8, uint16, uint32, uint64, uintptr
* byte // Alias for int8
* rune // Alias for int32, represents a unicode code point
* float32, float64
* complex64, complex128

Note: 
* To print the type of a variable use %T  in print statements
* To print the value use %v
* Every variable has a zero value
* In package scope only variables with var keyword can be used
* * At package scope all declarations should start with a keyword 
* package scoped vars lives until the program exits
Example: 
```go
var (
	ToBe   bool       = false
	MaxInt uint64     = 1<<64 - 1
	z      complex128 = cmplx.Sqrt(-5 + 12i)
)
func main() {
	fmt.Printf("Type: %T Value: %v\n", ToBe, ToBe)
	fmt.Printf("Type: %T Value: %v\n", MaxInt, MaxInt)
	fmt.Printf("Type: %T Value: %v\n", z, z)
}
```
#### Handling unused variables
* throws an error
* making maintenance easier
* only works for block scoped vars
* can be overriden by using blank operator
* _= <var_name>

#### Redeclaration
* short declaration can initialize a new var and assign value to exisiting vars
* Atleast one variable in the short declaration should be a new variable

#### Float inaccuracy problem

#### Type conversions
* The expression T(v) converts the value v to the type T.

Example:
```go
i := 42
f := float64(i)
u := uint(f)
```

#### Type Inference
* When declaring a var without specifying a type, var type is infered from the value.

Example:
```go
var i int
j := i // j is an int
```
* When the right hand side contains an untyped numeric constant, the new variable may be an int, float64, or complex128 depending on the precision of the constant:
```go
i := 42           // int
f := 3.142        // float64
g := 0.867 + 0.5i // complex128
```
### Constants
* Constants are declared like variables, but with the const keyword.
* Constants cannot be declared using the := syntax.

## Package
* All package files should be in the same directory
* All files should belong to the same package
* An executable package should be named as main
* package clause shouldn't be repeated
* Each go package has its own scope
### Library vs Executable
* Only 2 kinds of packages in go
* Executable package should be named main
* Should have a main() function
* Created only for reusability
* Cannot import an executable package
* Library package can have any name
### Scope
#### File scope
* only visible in a file
* example import
#### package scope
* Visible in all files belonging to the same package
* Other packages can only access them if it is imported
#### Block scope
* Visible inside a block

## Importing
* Only file scoped

## Statements
* Statements control execution flow
* Go add ; between stmts automatically
* Can have multiple statements in a single line by seperating them using ;

## Expressions
* computes one or multiple statments
* should be used with or within a statments

## Commenting
// and /* */

## Go doc for documentation
### Note: use godocc package for colorful comments
* start comment with name of declaration

## Writing Libraries
### Exporting function or vars
* Make the first letter uppercase letter


## Slices
* can store multiple values
* itself has a single value
* each value in a size is an unnamed var
* A pointer basically
* Can use indexing
* len() get count

## Print Formatted Output
### Printf vs Println
* Printf prints formatted 
* Printf doesn't print new line
* Need to specify the verbs
* First arg is formatted string
* %-2d left align
#### Printf Formatting
* %q - quotted string 
* %v - value
* %d - digit
* %T - type

## String Conv
* strconv.ParseFloat
* strconv.Itoa
* strconv.FormatBool

## Raw String
* quotes with back quotes
* multi line
* type is string
* typed differently
* can be assigned to a string var

## Length of a string
* len() finds the num of bytes in a string
* to find length of unicode string, utf8.RuneCountInString()

# Bits & Bytes
* printing binary values fmt.Printf("%02b", 0) print 00
## Predeclared type
* built in type which can be used without any package
* has a name, representation and size
* string literals are encoded in UTF-8 only

## Handling overflows 
* Go can catch overflow errors at compile time
* Go cannot catch overflow errors in runtime
* Integers wraps around the value, ie; when added after max value it starts from min value and the other way around when subtract
* Floating point values goes to infinity +Inf or -Inf

## Defined Datatypes
* User defined data types
* can attach methods to the type
* Also called a Named Type
* Defined using type definition stmt
Example:
```go
type Duration int64
```
* Need to type conv if needed to use with base type
Example:
```go
var ms int64 = 1000
var ns Duration
ns = Duration(ms)
ms  = int64(ms)
```
* There is no type hierarchy in go
* A defined type and it's source type share the same underlying type
Example: 
```go
type Duration int64
type MyDuration Duration // Underlying type is int64
```
* This means the methods are also not inherited
* A type from a package is different from the one in your code even if they have the same name

## Aliased types
* Same type but different name
Example:
```go
type byte = uint8 // notice the = sign
type rune = int32 // rune represents a unicode char
```
* Not for everyday use
* Used only for refractoring
* Can be used in a statement without type casting


# Constants
## Unnamed Constant

## Named Constant
* No need to write the constants in caps
Example
```go
const pi float32 = 3.14
```
* using const go will be able to detect errors in compile time
## Rules of constants
* Need to initilize in the time of declaration
* Cant assign values
* Cannot initialize const to a runtime value
Example:
```go
const max int = math.Pow(2,4) // Error

func main() {
    n := 10
    const max in = n // Error
}
```
* Exception to this rule is len() when it uses a const value as arg
Example 
```go
const max int = len("Hello")
```
* You don't have to declare the types of a const
* If you don't specify a type; it becomes a typeless const
* You can use expression during const declaration

Examples
```go
func main() {
    const min = 1 + 1
    const pi = 3.14 * min // works
    const version = "abc" + "efg"
    const debug = !true
}
```

### Multiple const declaration
```go
const min, max int = 1, 1000
// or
const (
    min int = 1
    max int = 1000
)
```
* If you don't specify type or expression after the first const it will automatically assign it from the previous one
```go
const (
    min int = 1
    max
)
```
## Untype constants
* when you declare a const without type it becomes a untyped const (typeless const)
* type is assigned only during execution
* Rune literal is defined using single quotes
* methods can only be defined on a defined type

## iota()
* number generator for constants
* counter starts at 0
* used with _ in case to skip a value
Example
```go
func main() {
    const(
        monday = iota
        tuesday
        wednesday
        thursday
        friday
        saturday
        sunday
    )
    const(
        EST = -(5 + iota)
        _
        MST
        PST
    )
}
```

## Naming conventions
* use short vars in small scopes (concise and idiomatic)
* * use first few letters of words
* * var s string // string
* * var i int // index
* * var n,num int // number
* * var m // another number
* * var msg string // message
* * var v, val string // value
* * var fv string // flag value
* * var err error // error value
* * var args []string // arguments
* * var seen bool // has seen?
* * var parsed bool // parsing ok?
* * var buf byte[]// buffer
* * var off int // offset
* * var op int // operation
* * var readOp int // read operation
* * var l int // length
* * var c int // capacity
* * var a int // array
* * var r rune // rune
* * var sep string // seperator
* use camelcase
* use all caps for acronyms
* Do not stutter
Example
```go
player.PlayerScore // bad
player.Score // good
```
* Do not use underscore

# Error Handling
## nil value
* value is not initialized yet
* zero value for
* * pointers
* * slices
* * maps
* * interfaces
* * channels
* error value will be nil if no errors occured
## error value
* no try catch stmt
* some functions never fails
* * strconv.Itoa
* function which returns an error looks like this 
```go
func func_name(args) (return_values, error)

ret, err := func_name(args)
```
* error is a type

# Array
* Stores data in consequent memory
* Efficient: CPU Cache Lines, Fast access 1-o-1 representation of memory
* Size of an array = sum of sizes of all elements
* length of an array should be constant
* element type has to be declared
* zero values of element type gets assigned automatically
* `any` type can be defined as placeholder type
* Once it's assigned the type cannot be changed
* Each element of array is an unnamed var
* go doesn't allow direct access to arbitary memory locations
```go
    var books [4]string {"A", "B", "C", "D"}
    // Or
    books := [4]string{"A",
        "B", 
        "C", 
        "D",
    }
```
* Array literals are enclosed in {} seprated in commas (element list)
* One of the composite literals
* you can use [...] when assigning values, it automatically calculates the length of array
* It's called ellipses
```go
    books := [...]string{"A","B"} // len(books) = 2
```
### Array comparison
* Identical types [len]elmt_type
* Element types need to comparable
* length of an array is an inseparable part of it's type

## Multi Dimentional Array (Array of arrays)
```go
    arr := [2][3]int{
		{2, 3, 4},
		{5, 6, 7},
	}
	for _, v1 := range arr {
		for _, v2 := range v1 {
			fmt.Println(v2)
		}
	}
```

## Keyed elements
```go
    arr := [3]float {
        0: 1.2,
        1: 4.5,
        2: 5.4,
    }
```
* Order of the keys doesn't matter
* You can skip indices and it will get assigned zero value
* Keyed element can determine the index of unkeyed elements
* Unkeyed elements are appeneded to the end of array in the order of def

## Unamed type & Named type
* An unnamed type is it's own element type
Example:
```go
    [3]int {1,2,3}
```
* A named type has a type definition
Example:
```go
    type arrType [3]int
    arr := arrType { 1,2,3 }
```
* A named type array can be compared with an unname array as long as they have the same undelying type
* You can't compare 2 different named type even if they have the same underlying type directly
* You can type conv named types if their undelying type is the same

# Slices
* Slices has array as a backend
* default value is nil
* dynamic sized
* len() is dynamic
* has both len() and cap()
Example:
```go
    slice := []int {1,2,3}
```
* slices are not inter comparable
* slices can only be compared to nil value
* You can inter assign slices as long as they have same elemental data type
* nil slice and empty slice are not the same
* Empty slice is an initialized slice
* Go prefers slices over array when it comes to functions since it's dynamic
## append function
```go
    slice := []int {1,2,3}
    slice2 := []int {7,8,9}
    slice = append(slice, 4)
    slice = append(slice, 5, 6)
    // ellipsis(...) allows concatanation of slices
    slice = append(slice, slice2...)
```

## Slice expression
* A slicable value is any value that can be sliced using a slice expression
* slice, array, string are slicable
```go
    arr[START:STOP]
    // default values
    arr[0: len(arr)]
    // Same as
    arr[:] 
```
* Slicing a slice is called reslicing
* Slice expression return a slice with the same type of sliced value

## Backing array
* a slice doesn't directly store any elements
* A backing array is a hidden array created by a slice and it refers to that array
* A slice is a window to it's backing array
* Changing an element in a slice causes all slices refering that window to change
* Slicing operations are such fast & cheaper than array operation
Creating a new slice from existing slice with a different backing array 
```go
    grades := []float64{10,20,30,40,50}
    newGrades := append([]float64(nil), grades...)
```
## Slice header
* Slice refers to the backing array through memory address
* Go implements slice as a sliceHeader datastructure
* It has the following fields
* * pointer
* * length: length of the slice
* * capacity: legth of backing array
* A slice cannot index the elements beyond it's length (without reslicing)
* Elements located before the pointer are not visible to the slice
* If you don't know whether a slice will ever have a value declare it as a nil slice
* It doesn't have a backing array but has a slice header with nil pointer

* When using slices in functions the value changes since function only creates a copy of sliceHeader not the backing array

## Capacity of Slice
* You cannot extend a slice beyond it's capacity
* you can grow a slice to it's capacity
* Capacity reduces when you slice after the first element
* It's because the pointer value changes
* You cannot extend a slice back once you lost the original header

## Append function and capacity
* When capacity is full the append function allocates another backing array for the slice
* The append function returns a new slice header for the new backing array
* An append function always creates a larger array than required in order to reduce the array recreation
* Append function set zero values to uninitialized elements of the array
* Append function doubles the capacity whenever it needs to allocate a larger array until the cap is 1024
* After that it will grow it a little bit slower

## Full Slice expressions
* Allow you to control size of a returned slice
```go
    sl := oldSl[BEGIN:END:CAP]
```
* END pos cannot be greater than cap
* CAP means relative position of last element accessible in the backing array
```go
// The following slice's capacity will be 2 even though 3 is specified
    sl := oldSl[1:3:3]
```
* This can be used to make sure that append() doesnt override the values in the backing array

## make()
* allows you to pre allocate a slice with fixed backing array size and capacity
```go
    make([]int, 3)
```
* returned slice's cap and len are equal
* To create a slice with different len and cap
```go
    make([]int, 3, 6)
```
* Use make() only for optimization or to pass limitted slice to func eg; io.Read()

## copy()
* copy elements of a slice to another
```go
    copy(dest, src)
```
* should have identical element types to work
* copies the elements based on the len of smallest slice
* it return an integer value notifying how many elements were copied
* You can't create new elements using copy(), it's limited to the length of dest slice

## Multi dimentional slices
```go
    slice := [][]int {
        { 1, 2, 3},
        { 1 },
        { 1, 2 },
    }
```

# File operations
* os.FileInfo() is an interface which contains the file properties
* ioutils package has all file operations
* for docs run
```bash
go doc ioutil
```
## Writing to a files
* ioutils.WriteFile() writes a byte slice into a file
* to convert a string element to byte use ellipsis(...) operator
* To calculate required size of an array there are 2 methods:
1. Use a heuristics: Multiply number of files with reserved avg filename lens across fs
* Simple but waste unnecessary memory
* maximum filename length across common fs is 255 bytes
2. Actually calculate file name lengths
* * Memory efficient but wastes more cpu time


## Unicode encoding
* Bytes Runes String
* []byte and string are interchangeably convertible
* Rune is a Unicode Code Point
* Representing number systems
* * 0b - binary
* * Ox - hexa
* * O - octa
* string(n) encodes given rune to a utf-8 encoded string value automatically
* In ASCII 0-32 are control codes cannot be printed
* Each ASCII rune is 1 byte
* Emojies are 128512 128591
* A rune literal can store upto 4 bytes
* A string value is a read only byte slice
* To modify convert a string value to []byte
* Changing a byte slice won't affect the original string
* Go creates a new slice and copies the element into the new []byte
* Mirror functions help you to prevent converting []byte to string and vice versa(Type conversions are costly)
* UTF-8 encoding is variable length encoding for efficiency
* This causes indexes to skip some values because of it's byte length
* To get correct charcters in incremental indices use []rune
* Using []rune array is costly
* string literals are automatically encoded in utf-8
## Rune Decoding
```go
word = "Mäh"
for i, _ := range {
    
}
```

# Structs
* Allows us to represent concepts
* Has fields
* Derived data type
* Similar to classes in OOP langs
* Fixed at compile type
* Hetrogenous data type
* Go copies value when you assign a struct to another
* Go compares each fields when you compare 2 struct
* It's better to declare struct at package level
* You cannot compare struct values if they contain fields with uncomparable types like slice, map or func value
* A struct can have fields without a name (anonymous field)
* Go automatically assigns type to the field during runtime
* If the fields are repeated inside the inner struct, the outer field takes priority over the inner one
## Struct Embedding
* Instead of Inheritance(is-a relationship) go uses composition(Embedding) (has-a relationship)

# Maps
* Uses O(1) algorithm. ie; it spends const amount of time looking for a key
* KV pairs
* Keys should be unique
Example
```go
// create an initialized empty map
dict := map[string]string {}
dict["up"] = "down"
// Initilizing an empty map
maps := map[string]string {
    "a": "b",
    "c": "d",
}
```
* The key can be any comparable type( supports equality operator)
* If an element doesn't exist in a map, the map returns a zero value depending upon the element type of map
* You can't compare a map to another map or even itself
* Don't use float types as map key, results may be inaccurate
* You need to initialize a map before using it, you can read from it, but not write to it
* All keys and values in a map should belong to the same type
* Maps overrides the values if assigned with the same key
* A map also returns a boolean value which can detect if the key exists
Example:
```go
value, ok := dict["d"]
```
* Map doesn't store data in ordered fashion
* For fast traversal use slice instead of map
## Ranging over a map
```go
for k,v := range dict {
    fmt.Printf("%s -> %s", k, v )
}
```
## Internals
* Map variable is a pointer to a map header
* A map header is created behind the scenes
* Assignment copies the map header and doesn't create a copy of the map
* The map header contains another pointer to the real data
* Map header is a complex data structure with lots of moving parts
* If you want an actual copy of a map, use make to create a new map, and copy the elements

## Delete
* the delete func deletes a key value pair from a map
Example:
```go
delete(mapValue, key)
```
### Clearing an entire map
* Behind the scenes go convert this entire operation into a single mapclear() command
```go
for k := range mapValue {
    delete(mapValue,k )
}
```
### Retrieving an element
```go
elem, ok = m[key]
```

# Reading input
* The bufio.Scanner package is used for reading user inputs
* It scan a stream of input data line by line and store it in a buffer
* It can receive the input stream from the command line or a network source
* By default the scanner stops when it receives a newline char
* There are 2 methods, Scan() and Text() methods
* Scan() returns false when there is an error or no more data to scan
* It returns true if there is data to scan
Example:
```go
    in := bufio.NewScanner(os.Stdin)
	in.Scan()
	fmt.Println(in.Text())
```
* Only the last scanned line is available on the buffer
* The scanner has an error method which can handle errors
* You don't have to define it everytime

# Sets
* Unique data

# Json
* Encoding is called Marshaling
* Decoding is called Unmarshaling
* json package only encodes exported fields
* MarshalIndent() prints pretty format
* You need to pass a pointer to Unmarshal()
## Field tags
* Metadata about a field
* Key value pair, key usually denote a package name(like json)
* A package only reads a field tag if it starts with the package name as key
* `json:"-"` hides a field
* `json:"blah"` encodes using a different field name (blah in this case)
* `json:",omitempty"` omits if empty