# Writing Web Applications

## Templating
* text template (text/template)
* html tempate (http/template)

# Template Caching
* The function template.Must() is a convenience wrapper that panics when passed a non-nil error value, and otherwise returns the *Template unaltered.

# URL matching Regex
https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)