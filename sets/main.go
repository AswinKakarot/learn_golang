package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strings"
)

func main() {
	var words = map[string]bool{}
	in := bufio.NewScanner(os.Stdin)
	in.Split(bufio.ScanWords)
	rx := regexp.MustCompile(`[^a-z]+`)
	for in.Scan() {
		word := strings.ToLower(in.Text())
		word = rx.ReplaceAllString(word, "")
		if len(word) > 2 {
			words[word] = true
		}
	}

	if err := in.Err(); err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(len(words))
}
