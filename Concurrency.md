# Concurrency
* Implemented using goroutines
* Goroutines are light weight threads managed by the go runtime
```go
go f(x,y,z)

func f(x,y,z int) {}
```
* The evaluation of f, x, y, and z happens in the current goroutine
* execution of f happens in the new goroutine.
* Goroutines run in the same address space, so access to shared memory must be synchronized.
* The sync package provides useful primitives
* you won't need them much in Go as there are other primitives.

# Channels
* Channels are a typed conduit through which you can send and receive values with the channel operator, <-
```go
ch <- v    // Send v to channel ch.
v := <-ch  // Receive from ch, and
           // assign value to v.
```
* Data flows in the direction of the arrow
* Like maps and slices, channels must be created before use
```go
ch := make(chan int)
```
* By default, sends and receives block until the other side is ready.
* This allows goroutines to synchronize without explicit locks or condition variables.

## Buffered Channels
* Channels can be buffered. Provide the buffer length as the second argument to make to initialize a buffered channel
```go
ch := make(chan int, 100)
```
* Sends to a buffered channel block only when the buffer is full. Receives block when the buffer is empty.

## Length and capacity of channel
* When we run a make() to create a channel we set the capacity of the channel
* The length of the buffered channel is the number of elements currently queued in it.

#S Range and close
* A sender can close a channel to indicate that no more values will be sent.
* Receivers can test whether a channel has been closed by assigning a second parameter to the receive expression
```go
v, ok := <-ch
```
* The loop for i := range c receives values from the channel repeatedly until it is closed.
* Only the sender should close a channel, never the receiver. Sending on a closed channel will cause a panic.
* Channels aren't like files; you don't usually need to close them.
* Closing is only necessary when the receiver must be told there are no more values coming, such as to terminate a range loop.

# Select
* The select statement lets a goroutine wait on multiple communication operations.
* A select blocks until one of its cases can run, then it executes that case.
* It chooses one at random if multiple are ready.
* The default case in a select is run if no other case is ready.
* Use a default case to try a send or receive without blocking
```go
select {
case i := <-c:
    // use i
default:
    // receiving from c would block
}
```

# Mutex
* to make sure only one goroutine can access a variable at a time to avoid conflicts
* This concept is called mutual exclusion
* the conventional name for the data structure that provides it is mutex.
* Go's standard library provides mutual exclusion with sync.Mutex and its two methods:
* * Lock
* * Unlock
* We can use defer to ensure the mutex will be unlocked as in the Value method.