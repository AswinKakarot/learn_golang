package main

import (
	"fmt"
	"strconv"
	"time"
)

func main() {
	for {
		// Clear Screen \f
		fmt.Print("\033[2J")
		// fmt.Print("\f")
		// Move cursor to the top left position
		fmt.Print("\033[H")
		printTime(time.Now())
		time.Sleep(time.Second)
	}

}

func printTime(t time.Time) {
	s := fmt.Sprintf("%.2d", t.Second())
	m := fmt.Sprintf("%.2d", t.Minute())
	h := fmt.Sprintf("%.2d", t.Hour())
	curTime := h + "-" + m + "-" + s
	for i := 0; i < 5; i++ {
		for j := 0; j < 8; j++ {
			index := 0
			if string(curTime[j]) == "-" {
				sec, _ := strconv.Atoi(s)
				if sec%2 == 0 {
					index = 11
				} else {
					index = 10
				}

			} else {
				index, _ = strconv.Atoi(string(curTime[j]))
			}
			fmt.Printf("%-5s", digits[index][i])
		}
		fmt.Println()
	}
}
