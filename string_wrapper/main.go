package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"unicode"
)

const (
	usage    = "Usage: ./wrapper <in_filename> <out_filename>"
	maxWidth = 40
)

func main() {
	args := os.Args[1:]
	if len(args) != 2 {
		fmt.Println(usage)
	}
	var (
		buf []byte
		out []string
		lw  int
	)
	buf, err := ioutil.ReadFile(args[0])
	if err != nil {
		fmt.Println(err)
		return
	}

	for _, r := range string(buf) {
		out = append(out, string(r))
		switch lw++; {
		case lw > maxWidth && r != '\n' && unicode.IsSpace(r):
			out = append(out, string("\n"))
			fallthrough
		case r == '\n':
			lw = 0
		}
	}
	outBuf := []byte(strings.Join(out, ""))
	ioutil.WriteFile(args[1], outBuf, 0644)
}
