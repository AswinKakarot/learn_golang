# OOP Concepts in Go
* There is no concept of class
* You can attach methods to all types including basic types
* You can call methods on nil value of the type as well

# Methods
* enhance type with additional behavior
* Methods of a type are called method set of the type
* A method has a receiver which is an input param written before a fn's name
Example
```go
func (b book) printBook() {
    
}
```
* Method names can be reused across multiple types
* A method expression allows you to call a method through a type
Example
```go
book.print(mobyDick)
```
* Internally method is a function with a receiver as the first parameter
* A method belongs to a type where as a function belongs to a package
* A receiver is a copy of the value
* You can use a pointer receiver to modify values of the type value
* You don't need to specify the address of the value while calling the method
* Go will automatically take the address when a method has a pointer receiver
* When one method takes pointer receiver it's better to convert all methods to pointer receiver
* Use pointer receiver when working with large values in methods

# Interfaces
* Decouples different types from each other
* It's a protocol or a contract
* Abstract type with no implementation
* Describes expected behavior from a type
* It's the opposite of a concrete type
* All types except for interfaces in go are concrete types
* It's a convention to name the interfaces ending with "er"
* An interface type contains methods without implementations
* The types using the interface implements the method
* Go does not have an "implements" keyword
* A type satisfies an interface automatically when it has all the methods of the interface without explicitly specifying it
* Use single or only a few methods in an interface otherwise it will be difficult to satisfy it
* Bigger the interface, weaker the abstraction
* Interface values are comparable
* Go interfaces are implicit.
* Implementing type don't require to specify that they implement an interface
## Type assertion
* Extract dynamic value inside an interface value
* Zero value of an interface is nil
* An interface value wraps and hides the dynamic value
* This allows interface to isolate or decouple one part of the code from another
* Wrapped value inside an interface value is called a dynamic value
* It's called dynamic value since it can change during runtime
* ie; you can assign a different value as long as it satisfies the interface
Syntax:
```go
    iName.(* type)
    p.(book)
```
## Empty Interface
* An empty interface is an interface without any methods
* Every type satisfies it, ie; it can represent any type of value
Example:
```go
    var e interface{}
```
* If you assign a different variable with methods to it, you can no longer use the methods in it
* You can't directly use the dynamic value of empty interface value
* You need to use assertion to get the value in the empty interface and then run operations
Example:
```go
    var e interface{}
    e = 2
    e = e.(int) * 15
    fmt.Println(e)
```
* You can also assign a slice to an empty interface value

## Promoted fields & methods
* In a struct, Fields without name is called anonymous or embedded.
* Name of the type is then used as field name
* Since struct has a requirement to have unique field names we can't do 
```go
import (
    "net/http"
)type Request struct{}type T struct {
    http.Request // field name is "Request"
    Request // field name is "Request"
}
```
* Fields and methods of anonymous (embedded) field are called promoted. 
* They behave like regular fields but can’t be used in struct literals
* The parent type has priority over childs


Note: When a type anonymously embeds a type, it can use the methdos of embedded type as it's own

## Existing Interfaces
* Common interfaces created by community
### Stringer
* An interface which customize the way a type represents itself as a string
* Can be used along with func in fmt package
```go
type Stringer interface {
    String() string
}
```
* To use it, create a String() method, use Sprintf() and return the string

### Sorter
* I require the following 3 methods for it to work
* The type which matches the sort Interface(ie, has the following methods) can be sorted
* sort.Sort(var) can be used to sort
* sort.Sort(sort.Reverse(var)) can be used for reverse sort
```go
type Interface interface {
    // Len is the number of elements in the collection.
    Len() int
    // Less reports whether the element with
    // index i should sort before the element with index j.
    Less(i, j int) bool
    // Swap swaps the elements with indexes i and j.
    Swap(i, j int)
}
```

### Marshalers
* A method called MarshalJSON() is required to modify the json output
* An Encoder calls MarshalJSON() if type is a json.Marshaler
* A method called Unmarshal() is required to modify the json input
* A decoder calls UnmarshalJSON() if the type is json.Unmarshaler 

### [https://blog.golang.org/error-handling-and-go] Errors
* The error type is an interface type.
* An error variable represents any value that can describe itself as a string.
* The error type, as with all built in types, is predeclared in the universe block.
```go
type error interface {
    Error() string
}
```
* The fmt package formats an error value by calling its Error() string method.

### Readers
* The io package specifies the io.Reader interface, which represents the read end of a stream of data
* It has a Read() method
```go
func (T) Read(b []byte) (n int, err error)
```
* Read populates the given byte slice with data and returns the number of bytes populated and an error value. 
* It returns an io.EOF error when the stream ends.
