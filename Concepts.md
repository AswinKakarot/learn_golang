# Pointers
* dereference operator (*) value of(add)
* reference operator (&) address of(var)
## Stacks
* a goroutine is created everytime you run a code
* a goroutine is an independent path of execution of a program
* Can be considered as a light weight thread
* A part of stack called frame is allocated everytime a function is called
* After function completes executing this frame is freed from memory
* main() function also gets a frame
* active frame: the frame of currently executing function
* a gorouting can work in a single frame
* it can't access memory locations in other frames directly
* Isolation of frames provides immutability
* This reduces the efficiency since data needs to be copied to other frames during entering and exiting a function
* Doesn't scale well when big amount of data is passed as an argument and returned to a function

### Passing pointers as arguments
* Pointers can be used to reference variables outside a frame
```go
func sqrAddr(p *int) {
    *p *= *p
    return
}

func main() {
    a := 4
    sqrAddr(&a)
    fmt.Println(a)
}
```

### Return pointer from a function
* When returning a pointer from a function the stack frame get cleared after execution of that function
* In such cases go utilizes heap memory to allocate the memory and returns the address in heap
* Heap memory requires special garbage collection since it's not automatically cleared like stack
* Too much garbage collection will effect efficiency

```go
type person struct {
    name string
    age int
}

func initPerson() *person {
    m := person{ name: "Noname", age: 50 }
    return &m
}

func main() {
    fmt.Println(*initPerson())
}
```

Ref: https://www.youtube.com/watch?v=sTFJtxJXkaY

# Linked Lists
* Not indexed
* Traversal is hard O(n)
* Adding or deleting elements to beginning is easy O(1)
## V/S Array
* To add an element to an array all elements has to shift one position to right

## Doubly linked list
* Adding or deleting elements at beginning and end are easy