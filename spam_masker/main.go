package main

import (
	"fmt"
	"os"
)

// func main() {
// 	text := strings.Join(os.Args[1:], " ")
// 	if len(text) == 0 {
// 		fmt.Println("Give me something to mask!")
// 		return
// 	}
// 	var (
// 		size = len(text)
// 		buf  = make([]byte, 0, size)
// 	)

// 	const (
// 		link  = `http://`
// 		nlink = len(link)
// 	)
// 	for i, j, flag := 0, 0, false; i < size; i++ {
// 		buf = append(buf, text[i])
// 		if flag {
// 			if buf[i] == ' ' {
// 				flag = false
// 				continue
// 			}
// 			buf[i] = '*'
// 		}
// 		if buf[i] == link[j] && j <= nlink {
// 			j++
// 		} else if j != 0 && !flag {
// 			j = 0
// 		}
// 		if j == nlink {
// 			flag = true
// 			j = 0
// 		}
// 	}
// }

func main() {
	args := os.Args[1:]
	if len(args) != 1 {
		fmt.Println("gimme somethin' to mask!")
		return
	}

	const (
		link  = "https://"
		nlink = len(link)
	)

	var (
		text = args[0]
		size = len(text)
		buf  = make([]byte, 0, size)
		flag bool
	)

	for i := 0; i < size; i++ {
		// slice the input and look for the link pattern
		// do not slice it when it goes beyond the input text's capacity
		if len(text[i:]) >= nlink && text[i:i+nlink] == link {
			flag = true
			buf = append(buf, link...)
			i += nlink
		}
		c := text[i]
		switch c {
		case ' ', '\t', '\n':
			flag = false
		}
		if flag {
			buf = append(buf, '*')
			continue
		}
		buf = append(buf, c)

	}

	// print out the buffer as text (string)
	fmt.Println(string(buf))
}
