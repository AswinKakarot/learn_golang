package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
)

const usage = `grep utility
usage: ./grep pattern < file
`

func main() {
	args := os.Args[1:]
	if len(args) != 1 {
		fmt.Println(usage)
		return
	}
	in := bufio.NewScanner(os.Stdin)
	in.Split(bufio.ScanLines)
	rx := regexp.MustCompile("(?i)" + args[0])
	for in.Scan() {
		line := in.Text()
		if ok := rx.MatchString(line); ok {
			fmt.Println(line)
		}
	}
}
