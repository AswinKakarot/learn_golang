package main

import (
	"fmt"
	"os"
	"strconv"

	"gitlab.com/AswinKakarot/learn_golang/library/prime"
)

func main() {
	args := os.Args[1:]
	for _, str := range args {
		i, err := strconv.Atoi(str)
		if err != nil {
			continue
		}
		if prime.IsPrime(i) {
			fmt.Println(i)
		}
	}
}
