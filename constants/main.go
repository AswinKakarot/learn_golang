package main

import (
	"fmt"
)

func main() {
	const min = 1 + 1
	const pi = 3.14 * min // works
	const version = "abc" + "efg"
	const debug = !true
	fmt.Printf("min: %d \npi: %g \nversion: %v \ndebug: %v", min, pi, version, debug)
}
