# Pointers
* Stores memory address
* Pointers has type in go
* zero value is nil
* %p is used to print address of a pointer

## Pointer and composite types
* A slice header bears a pointer to backing array
* You don't need to use a pointer to a slice to modify it's elements
* But to add an element you need to specify it as a pointer (Do not use pointers with slices)
* In a map, it will add the data unlike slices
* In a struct you need to specify a pointer to change the element inside a function
* Go automatically puts an indirection operator in the case of maps, structs, slices and arrays
* You can find address of a struct, slice and array element but not a map element