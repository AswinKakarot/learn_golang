package main

import (
	"fmt"
	"os"
)

const (
	username string = "user"
	password        = "password"
)

func main() {
	if len(os.Args) != 3 {
		fmt.Println("Usage: [username] [password]")
		return
	}

	if os.Args[1] != username {
		fmt.Println("Access denied for", os.Args[1])
	} else if os.Args[2] != password {
		fmt.Println("Invalid password for", os.Args[1])
	} else {
		fmt.Printf("acess granted for %s!", os.Args[1])
	}
}
