package main

import (
	"fmt"
	"runtime"
)

func main() {
	fun_if(10, 6)
	// Print number of threads
	fmt.Println(runtime.NumCPU())
}
