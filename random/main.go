package main

import (
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"time"
)

func main() {
	n, _ := strconv.Atoi(os.Args[1])
	rand.Seed(time.Now().UnixNano())
	for c := 0; c < 5; c++ {
		i := rand.Intn(n + 1)
		if i == n {
			fmt.Println("You win!!")
			return
		}
	}
	fmt.Println("You lost!")
}
