package main

import (
	"fmt"
	"strings"
)

func main() {
	wizards := [6][3]string{
		{"First Name", "Last Name", "NickName"},
		{"Albert", "Einstein", "time"},
		{"Isaac", "Newton", "apple"},
		{"Stephen", "Hawking", "blackhole"},
		{"Marie", "Curie", "radium"},
		{"Charles", "Darwin", "fittest"},
	}
	for i, w := range wizards {
		fmt.Printf("%-15s %-15s %-15s\n", w[0], w[1], w[2])
		if i == 0 {
			fmt.Println(strings.Repeat("=", 50))
		}
	}
	fmt.Println(strings.Repeat("-", 50))
}
