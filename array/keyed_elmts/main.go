package main

import "fmt"

func main() {
	const (
		BTC = iota
		ETH
		WAN
	)
	rates := [...]float32{
		BTC: 1,
		ETH: 0.5,
		WAN: 0.18,
	}
	fmt.Printf("ETH is %v BTC\n", rates[ETH])
	fmt.Printf("WAN is %v BTC\n", rates[WAN])
}
