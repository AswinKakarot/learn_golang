package main

import (
	"fmt"
	"os"
	"strings"
)

func remove(slice []string, s int) []string {
	if len(slice) < s+1 {
		return nil
	}
	return append(slice[:s], slice[s+1:]...)
}

const corpus = "lazy cat jumps again and again and again since the beginning this was very important"

const ignWords = "and or was the since very"

const usage = "Please give me a word to search."

func main() {
	if len(os.Args) < 2 {
		fmt.Println(usage)
		return
	}
	query := strings.Fields(strings.ToLower(strings.Join(os.Args[1:], " ")))
	ign := strings.Fields(ignWords)
	for _, q := range query {
		for i, w := range ign {
			if q == w {
				query = remove(query, i)
			}
		}
	}
	words := strings.Fields(corpus)
	for i, w := range words {
		for _, q := range query {
			if w == q {
				fmt.Printf("#%-3d: %q\n", i, w)
			}
		}
	}
}
