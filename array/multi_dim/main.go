package main

import "fmt"

func main() {
	arr := [2][3]int{
		{2, 3, 4},
		{5, 6, 7},
	}
	for _, v1 := range arr {
		for _, v2 := range v1 {
			fmt.Println(v2)
		}
	}
}
