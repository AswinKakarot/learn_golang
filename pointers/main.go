package main

import "fmt"

func sqrAddr(p *int) {
	*p *= *p
	return
}

type person struct {
	name string
	age  int
}

func initPerson() *person {
	m := person{name: "Noname", age: 50}
	return &m
}

func main() {
	a := 4
	sqrAddr(&a)
	fmt.Println(a)
	fmt.Println(*initPerson())
}
