package main

import (
	"fmt"
)

var c, python, java bool

func main() {
	var i int
	var x, y = 10, 15
	// Short Variable declaration
	k := 3
	fmt.Println(i, c, python, java)
	fmt.Println(x, y, k)
}
