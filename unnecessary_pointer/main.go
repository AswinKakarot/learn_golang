package main

import "fmt"

type Students []string

type School map[int]string

func main() {
	schools := make([]School, 2)
	for i := range schools {
		schools[i] = make(School)
	}
	load(schools[0], Students{"batman", "superman"})
	load(schools[1], Students{"spiderman", "wonder woman"})
	fmt.Println(schools[0])
	fmt.Println(schools[1])
}

func load(m School, s Students) {
	for i, name := range s {
		m[i+1] = name
	}
}
