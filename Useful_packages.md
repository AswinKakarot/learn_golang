# fmt
## Println
## Printf
## Print
## Sprintf doesn't print to console but returns a formatted string
# math
## rand
### Intn
### Seed
## cmplx
# runtime
## NumCPU
# os
## Argv
# strings
## ToLower
## TrimSpace
## TrimRight
## Repeat
# strconv
## Itoa : int to string
## FormatBool : bool to string

# unicode/utf8
## RuneCountInString(str)
## RuneCount(bytes)
## DecodeRune(bytes)
## DecodeRuneInString(str)
# strings
## Fields : string split

# unsafe : Don't use in production
## Sizeof(): Calculate size of vars

# bytes
## ToUpper()