package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
)

func main() {
	var (
		visits  = map[string]int{}
		domains []string
	)
	in := bufio.NewScanner(os.Stdin)

	for in.Scan() {
		logLine := strings.Split(in.Text(), " ")
		website := logLine[0]
		visit, err := strconv.Atoi(logLine[1])
		if err != nil {
			fmt.Println("Incorrect log format, %s", err)
		}
		if _, ok := visits[website]; ok {
			domains = append(domains, website)
		}

		visits[website] += visit
	}
	sort.Strings(domains)
	for _, v := range domains {
		fmt.Printf("%-50s %d\n", v, visits[v])
	}
}
