package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	// os.Stdin.Close()

	in := bufio.NewScanner(os.Stdin)

	for in.Scan() {
		fmt.Println(in.Text())
	}

	if err := in.Err(); err != nil {
		fmt.Println(err)
	}
}
