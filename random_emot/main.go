package main

import (
	"fmt"
	"os"

	"gitlab.com/AswinKakarot/learn_golang/library/random"
)

const usage string = "Usage: ./rand_emot <name> [positive|negative]"

func main() {
	if len(os.Args) != 3 {
		fmt.Println(usage)
		return
	}
	emotion := ""
	emot := [...][3]string{
		{"happy", "awesome", "excited"},
		{"sad", "terrified", "angry"},
	}
	i := random.Int(3)
	switch os.Args[2] {
	case "positive":
		emotion = emot[0][i]
	case "negative":
		emotion = emot[1][i]
	default:
		fmt.Println(usage)
		return
	}
	fmt.Printf("%v is feeling %v!!", os.Args[1], emotion)

}
