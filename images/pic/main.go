package main

import (
	"image"
	"image/color"

	"golang.org/x/tour/pic"
)

// func Pic(dx, dy int) [][]uint8 {
// 	pp := make([][]uint8, dy)
// 	for y := 0; y < dy; y++ {
// 		p := make([]uint8, dx)
// 		for x := 0; x < dy; x++ {
// 			p[x] = uint8((x + y) / 2)
// 		}
// 		pp[y] = p
// 	}
// 	return pp
// }

type Image struct {
	x, y int
}

func (i Image) ColorModel() color.Model {
	return color.RGBAModel
}

func (i Image) Bounds() image.Rectangle {
	return image.Rect(0, 0, i.x, i.y)
}

func (i Image) At(x, y int) color.Color {
	v := uint8(x * y)
	return color.RGBA{v, v, 255, 255}
}

func main() {
	m := Image{x: 256, y: 65}
	pic.ShowImage(m)
}
