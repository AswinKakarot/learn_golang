package main

import (
	"fmt"
	"io/ioutil"
	"os"
)

const (
	usage   string = "Usage: ./file_finder <dir_name>"
	outFile string = "files/empty_files.md"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Println(usage)
		return
	}
	dirName := os.Args[1]
	files, err := ioutil.ReadDir(dirName)
	if err != nil {
		fmt.Println(err)
		return
	}
	var emptyFiles []byte
	for _, file := range files {
		if file.Size() != 0 {
			continue
		}
		emptyFiles = append(emptyFiles, file.Name()...)
		emptyFiles = append(emptyFiles, '\n')
	}
	err = ioutil.WriteFile(outFile, emptyFiles, 0664)
	if err != nil {
		fmt.Println(err)
		return
	}
}
