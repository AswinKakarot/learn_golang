package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strings"
)

func main() {
	in := bufio.NewScanner(os.Stdin)
	in.Split(bufio.ScanWords)
	// wordCount(*in)
	filterWordCount(*in)

}

func wordCount(in bufio.Scanner) {
	words := make(map[string]int)
	for in.Scan() {
		word := in.Text()
		words[word]++
	}
	for w := range words {
		fmt.Printf("%-15s %-15d\n", w, words[w])
	}
}

func filterWordCount(in bufio.Scanner) {
	total, words := 0, make(map[string]int)
	rx := regexp.MustCompile("[^a-zA-Z]+")
	for in.Scan() {
		total++
		word := strings.ToLower(rx.ReplaceAllString(in.Text(), ""))
		words[word]++
	}
	fmt.Printf("There are %d words, %d of them are unique.\n", total, len(words))
}
