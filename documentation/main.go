// Package main makes this program executable
package main

import "fmt"

/* main function
Go executes this function first
*/
func main() {
	fmt.Println("Documentation Sucks!!")
}
