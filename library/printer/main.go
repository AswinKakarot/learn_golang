package printer

import (
	"fmt"
	"runtime"
)

func PrintHello() {
	fmt.Println("Hello!")
}

func Version() string {
	return runtime.Version()
}
