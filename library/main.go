package main

import (
	"fmt"

	"gitlab.com/AswinKakarot/learn_golang/library/printer"
)

func main() {
	printer.PrintHello()
	fmt.Println(printer.Version())
}
