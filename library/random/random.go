package random

import (
	"math/rand"
	"time"
)

// Int returns a salted random number in range given in the args
func Int(n int) int {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(n)
}
