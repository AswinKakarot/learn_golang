package prime

// IsPrime returns true if number is prime
func IsPrime(n int) bool {
	switch {
	case n == 1:
		return false
	case n == 2 || n == 3:
		return true
	case n%2 == 0 || n%3 == 0:
		return false
	}
	i := 5
	w := 2

	for ; i*i <= n; i += 2 {
		if n%i == 0 {
			return false
		}
		w = 6 - w
	}
	return true
}
