# Control Statements
## Conditional
### if
* No paranthesis, but braces are required
* the if statement can start with a short statement to execute before the condition.
* Variables declared by the statement are only in scope until the end of the if.
* Variables declared inside an if short statement are also available inside any of the else blocks.
Example:
```go
func pow(x, n, lim float64) float64 {
	if v := math.Pow(x, n); v < lim {
		return v
	} else {
		fmt.Printf("%g >= %g\n", v, lim)
	}
	return lim
}
```

* break stmt
* continue stmt

### Switch
* fallthrough statement to not break the case
* it will execute the next case stmt without check

## Labled stmts
* Go to a certain line using continue or break
* label names does not conflict with var names
* has func scope
Example
```go
labl:
	for i := 0; i < 5; i++ {
		for j := 0; j < 10; j++ {
			fmt.Printf("i: %d \tj: %d\n", i, j)
			if i == j-i && i != 0 {
				break labl
			}
		}
	}
```
## Loops
### For loop
* C loop without paranthesis
* init and post stmts are optional
* no while loop in go, just use for and condition
* Infinite loop can be created by not specifying condition

### Range
* range stmt to loop over slices
```go
for _,v := range os.Args {
    fmt.Println(v)
}
```
## Defer
* A defer stmt defers the execution of a function until the surrouding function returns
* The deferred call's args are evaluated immediately, but the fuction call is not executed until the surrounding function returns
* Deferred func calls are pushed onto a stack. It's executed in LIFO order
* Defer is commonly used to simplify functions that perform various clean-up actions.
* Deferred functions may read and assign to the returning function's named return values.

Example:
```go
package main

import "fmt"

func main() {
	defer fmt.Println("world")

	fmt.Println("hello")
}
```
## Panic
* Pani is a built-in func that stops the ordinary ctrl flow and begins panicking
* When a func f calls pani, excution of f stops, any deferred functions in f are executed normally, then f returns to it's caller
* To the caller, f then behaves like a call to panic
* Tge process continues up the stack untils all functions in the current goroutine have returned, at which point the program crashes
* Panics can be intiated by invoking panic directly
* They can also be caused by runtime errors, such as out-of-bounds array access

## Recover
* recover is a built-int func that regains control of a pnikcing goroutine
* recover is only useful inside deferred func
* During normal exec, a call to recover will return nil and have no other effect
* If the current goroutine is panicking, a cakll to recover will capture the value given to panic and resumes normal exec

Example
```go
package main

import "fmt"

func main() {
    f()
    fmt.Println("Returned normally from f.")
}

func f() {
    defer func() {
        if r := recover(); r != nil {
            fmt.Println("Recovered in f", r)
        }
    }()
    fmt.Println("Calling g.")
    g(0)
    fmt.Println("Returned normally from g.")
}

func g(i int) {
    if i > 3 {
        fmt.Println("Panicking!")
        panic(fmt.Sprintf("%v", i))
    }
    defer fmt.Println("Defer in g", i)
    fmt.Println("Printing in g", i)
    g(i + 1)
}
```
Ref: https://blog.golang.org/defer-panic-and-recover