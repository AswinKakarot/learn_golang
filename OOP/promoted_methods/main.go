package main

import "fmt"

func main() {
	var (
		mobydick    = &book{product{title: "moby dick", price: 10}, 118281600}
		minecraft   = &game{product{title: "minecraft", price: 20}}
		tetris      = &game{product{title: "tetris", price: 5}}
		rubik       = &puzzle{product{title: "rubik's cube", price: 5}}
		cards       = &toy{product{title: "Playing Cards", price: 5}}
		davinciCode = &book{product{title: "Davinci Code", price: 100}, "733622400"}
	)
	var store list
	store = append(store, minecraft, tetris, mobydick, rubik, cards, davinciCode)
	fmt.Println("Orginal Price")
	store.print()
	store.discount(0.5)
	fmt.Println("Discounted Price")
	store.print()
}
