package main

import (
	"fmt"
	"os"
	"strconv"
	"time"
)

type book struct {
	title     string
	price     float64
	published interface{}
}

func (b book) print() {
	p := formatSwitch(b.published)
	fmt.Printf("%-15s: $%.2f %-15s\n", b.title, b.price, p)
}

func format(v interface{}) string {
	if v, ok := v.(int64); ok {
		return time.Unix(v, 0).String()
	}
	if v, ok := v.(string); ok {
		if i, err := strconv.Atoi(v); err == nil {
			return time.Unix(int64(i), 0).String()
		} else {
			fmt.Println(err)
			os.Exit(1)
		}
	}
	return "unknown"
}

func formatSwitch(v interface{}) string {
	var t int64
	switch e := v.(type) {
	case int:
		t = int64(e)
	case string:
		if i, err := strconv.Atoi(e); err == nil {
			t = int64(i)
		} else {
			fmt.Println(err)
			os.Exit(1)
		}
	default:
		return "unknown"
	}
	return time.Unix(t, 0).Format("1993/01")
}
