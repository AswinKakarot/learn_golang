package main

import "fmt"

func main() {
	var (
		mobydick    = book{title: "moby dick", price: 10, published: 118281600}
		minecraft   = game{title: "minecraft", price: 20}
		tetris      = game{title: "tetris", price: 5}
		rubik       = puzzle{title: "rubik's cube", price: 5}
		cards       = toy{title: "Playing Cards", price: 5}
		davinciCode = book{title: "Davinci Code", price: 100, published: "733622400"}
	)
	var store list
	store = append(store, &minecraft, &tetris, mobydick, rubik, &cards, davinciCode)
	store.print()
	store.discount(0.5)
	fmt.Println("Discounted Price")
	store.print()
}
