package main

import "fmt"

func main() {
	var e interface{}
	e = []int{1, 2, 3}
	fmt.Println(e)
	e = map[int]bool{1: true, 2: false}
	fmt.Println(e)
	e = true
	fmt.Println(e)
	e = 2
	e = e.(int) * 15
	fmt.Println(e)
}
