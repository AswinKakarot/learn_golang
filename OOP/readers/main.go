package main

import (
	"fmt"
	"io"

	"golang.org/x/tour/reader"
)

type MyReader struct {
	i int64
	s string
}

// TODO: Add a Read([]byte) (int, error) method to MyReader.

func (r MyReader) Read(b []byte) (n int, err error) {
	if r.i > int64(len(r.s)) {
		fmt.Println(r.i, len(r.s))
		return 0, io.EOF
	}
	n = copy(b, "A")
	r.i += int64(n)
	return
}

func main() {
	reader.Validate(MyReader{})
}
