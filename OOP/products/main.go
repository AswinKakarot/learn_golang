package main

import (
	"fmt"
	"sort"
)

func main() {
	l := list{
		{title: "moby dick", price: 10, released: toTimestamp(118281600)},
		{title: "odyssey", price: 15, released: toTimestamp("733622400")},
		{title: "hobbit", price: 25},
	}

	l.discount(.5)
	fmt.Println(l)
	// sort.Sort(l)
	// fmt.Println(l)
	sort.Sort(byReleaseDate(l))
	fmt.Println(l)
}
