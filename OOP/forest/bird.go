package main

type bird struct {
	name    string
	canFly  bool
	count   int
	species string
}
