package main

import "fmt"

type animal struct {
	name    string
	count   int
	species string
}

func (a *animal) print() {
	fmt.Printf("%s %s %d", a.name, a.species, a.count)
}

func (a *animal) incr() {
	a.count++
}

func (a *animal) decr() {
	a.count--
}
