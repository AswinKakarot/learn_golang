package main

import "fmt"

type book struct {
	title string
	price float64
}

func (b *book) print() {
	fmt.Printf("Book: %s\tPrice: $%f\n", b.title, b.price)
}

func (b *book) discount(ratio float64) {
	b.price *= (1 - ratio)
}
func main() {
	mobyDick := book{
		title: "Moby Dick",
		price: 10,
	}
	mobyDick.print()
	mobyDick.discount(.5)
	mobyDick.print()
}
