package main

import (
	"fmt"
	"math"
)

type ErrNegativeSqrt float64

func (f ErrNegativeSqrt) Error() string {
	return fmt.Sprintf("math: square root of negative number %g", float64(f))
}

func Sqrt(x float64) (float64, error) {
	if x < 0 {
		return 0, ErrNegativeSqrt(x)
	}
	o := math.Sqrt(x)
	return o, nil
}

func main() {
	fmt.Println(Sqrt(2))
	fmt.Println(Sqrt(-2))
}
