package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	const (
		headers = "Location,Size,Beds,Baths,Price"
		data    = `New York,100,2,1,100000
New York,150,3,2,200000
Paris,200,4,3,400000
Istanbul,500,10,5,1000000`

		separator = ","
	)
	var (
		locations                  []string
		sizes, beds, baths, prices []int
		total                      [4]int
	)
	for _, v := range strings.Split(headers, separator) {
		fmt.Printf("%-15v", v)
	}
	fmt.Printf("\n%v\n", strings.Repeat("=", 75))
	rows := strings.Split(data, "\n")
	for _, row := range rows {
		for i, v := range strings.Split(row, separator) {
			switch i {
			case 0:
				locations = append(locations, v)
			case 1:
				size, _ := strconv.Atoi(v)
				sizes = append(sizes, size)
				total[i-1] += size
			case 2:
				bed, _ := strconv.Atoi(v)
				beds = append(beds, bed)
				total[i-1] += bed
			case 3:
				bath, _ := strconv.Atoi(v)
				baths = append(baths, bath)
				total[i-1] += bath
			case 4:
				price, _ := strconv.Atoi(v)
				prices = append(prices, price)
				total[i-1] += price
			}
			fmt.Printf("%-15v", v)
		}
		fmt.Printf("\n")
	}
	fmt.Printf("\n%v\n%-15v", strings.Repeat("=", 75), "")
	for _, t := range total {
		fmt.Printf("%-15.2f", float64(t)/float64(len(rows)))
	}
	fmt.Println()
}
